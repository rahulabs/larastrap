<x-larastrap::form>
    <x-larastrap::text name="hide" label="An Hidden Text" hidden />
    <x-larastrap::textarea name="area" label="A large Textarea" rows="20" />
    <x-larastrap::url name="address" label="An URL" data-foo="bar" />
    <x-larastrap::text name="bool1" label="Boolean Attribute" :foobar="false" />
    <x-larastrap::text name="bool2" label="Boolean Attribute" :foobar="true" />
    <x-larastrap::text name="bool3" label="Boolean Attribute" barbaz />
</x-larastrap::form>
