# Contributing to Larastrap

## Setup

To prepare your development environment, run

```
git clone https://gitlab.com/madbob/larastrap.git
cd larastrap
composer install
```

It is possible to use your local (and, eventually, modified) version of the library on any locally installed Laravel application, for example by adding it temporarily to your composer.json file.

```
{
    "repositories": [
        {
            "type": "path",
            "url": "/your/local/copy/of/larastrap"
        }
    ],
    "require": {
        "madbob/larastrap": "dev-master"
    }
}
```

## Tests

To execute unit tests, run

```
vendor/bin/phpunit tests
```

Please ensure that all tests pass before to submit your merge request, otherwise it will be blocked by automatic checks.

Actually the test suite is quite limited: more contributions, to cover more use cases - including the edge ones - are welcome!

## Code Style

The standard PHP code style is adopted. To automatically format the code you add or change, run

```
vendor/bin/php-cs-fixer fix src
```

Please ensure to execute this command before to submit your merge request, otherwise it will be blocked by automatic checks.

## Documentation

The documentation is published on https://larastrap.madbob.org/

The code of this website is available on the repository https://gitlab.com/madbob/larastrap-website

## Love

Do not forget to leave a Star on the GitLab repository!

Fun facts: the GitLab support on Composer and Packagist - the popular PHP package manager - has been added by the Larastrap's maintainer primarily to improve coverage for this library! See: https://github.com/composer/composer/pull/11734 - https://github.com/composer/packagist/pull/1408
