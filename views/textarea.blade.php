<x-larastrap::field :params="$params['field_params']">
    <textarea id="{{ $params['id'] }}" class="{{ $params['generated_class'] }}" name="{{ $params['actualname'] }}" {!! $params['serialized_attributes'] !!}>{!! $params['value'] !!}</textarea>
    @include('larastrap::partials.error', ['params' => $params])
</x-larastrap::field>
