<div class="{{ $params['generated_class'] }}" {!! $params['serialized_attributes'] !!}>
    @if($params['do_label'] != 'hide')
        <x-larastrap::label :params="$params['label_params']" />
    @endif

    {!! $params['input_wrap_tag_start'] !!}

    {{ $slot }}

    @if(filled($params['help']))
        <div class="form-text">
            {!! html_entity_decode($params['help']) !!}
        </div>
    @endif

    {!! $params['input_wrap_tag_end'] !!}

    @include('larastrap::appended_nodes', ['params' => $params])
</div>
