<div class="{{ $params['generated_class'] }}" id="{{ $params['id'] }}" {!! $params['serialized_attributes'] !!}>
    {{ $slot }}
    @include('larastrap::appended_nodes', ['params' => $params])
</div>
