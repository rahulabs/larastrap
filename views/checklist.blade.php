<x-larastrap::field :params="$params['field_params']">
    <div class="form-control-plaintext {{ $params['generated_class'] }}">
        @foreach($params['options'] as $option_value => $option)
            @php
                $option = $parseOption($option, $params);
            @endphp

            <div class="form-check" {{ $option->hidden ? 'hidden' : '' }}>
                <input class="form-check-input {{ $params['generated_class'] }}" type="checkbox" name="{{ $params['actualname'] }}" id="{{ $option->id }}" value="{{ $option_value }}" {{ in_array($option_value, $params['value']) ? 'checked' : '' }}>
                <label class="{{ $option->serialized_label_classes }}" for="{{ $option->id }}" {!! $option->serialized_label_attributes !!}>{{ $option->label }}</label>
            </div>
        @endforeach
    </div>

    @include('larastrap::partials.error', ['params' => $params])
</x-larastrap::field>
