@foreach($params['buttons'] as $btn)
    @php
        $type = $btn['element'] ?? 'larastrap::button';
        unset($btn['element']);
    @endphp
    <x-dynamic-component :component="$type" :params="$btn" />
@endforeach
