@if($params['appendNodes'] && empty($params['appendNodes']) == false)
	@foreach($params['appendNodes'] as $node => $pars)
		@if(is_array($pars))
			<x-dynamic-component :component="$node" :params="$pars" />
		@else
			{!! $pars !!}
		@endif
	@endforeach
@endif
