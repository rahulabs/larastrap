# Changelog

## [0.9.1] - 2024-05-28

### Added

- Tel, Search, Week and Month input types (thanks to @dansleboby !8)
- "prefix" global configuration
- a CONTRIBUTING file

### Changed

- dot notation for Input's "name" parameter (thanks to @dansleboby !8 and !10)
- rendering behavior

## [0.9.0] - 2024-05-16

### Added

- "squeeze" option for "formview" parameter in Form
- "container" parameter for Navbar
- "symbol" and "symbol_html" parameters for Pophelp

### Changed

- internal management of serializable attributes
- centralized template for error reporting
- behavior of Pophelp

### Removed

- "step", "min" and "max" parameters for Number and Range: belong to native HTML attributes

## [0.8.8] - 2024-05-07

### Added

- a changelog!
- "multiple" parameter for File (thanks to @dansleboby !7)
- "squeeze" value for Field's "do_label" parameter, acting as a shorthand for "squeeze" attribute #7

## [0.8.7] - 2024-05-05

## [0.8.6] - 2024-04-29

## [0.8.5] - 2024-04-08

## [0.8.4] - 2024-02-19

## [0.8.3] - 2023-12-01

## [0.8.2] - 2023-11-22

## [0.8.1] - 2023-11-18

## [0.8.0] - 2023-11-08

## [0.7.3] - 2023-09-19

## [0.7.2] - 2023-06-26

## [0.7.1] - 2023-05-25

## [0.7.0] - 2023-05-07

## [0.6.1] - 2023-05-01

## [0.6.0] - 2023-04-07

## [0.5.2] - 2023-04-04

## [0.5.1] - 2023-04-04

## [0.5.0] - 2022-12-10

## [0.4.4] - 2022-11-11

## [0.4.3] - 2022-07.03

## [0.4.2] - 2022-05-07

## [0.4.1] - 2022-03-28

## [0.4.0] - 2022-03-27

## [0.3.5] - 2022-03-22

## [0.3.4] - 2022-02-11

## [0.3.3] - 2022-02-07

## [0.3.2] - 2022-01-28

## [0.3.1] - 2022-01-27

## [0.3.0] - 2021-10-28

## [0.2.0] - 2021-10-07

## [0.1.1] - 2021-09-25

## [0.1.0] - 2021-08-30
