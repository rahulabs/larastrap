<?php

namespace MadBob\Larastrap\Base;

use Illuminate\Support\Str;

abstract class Container extends Element
{
    public $obj;
    private $slot;

    /*
        If an attribute for a Component is not explicit, it always tries to
        convert into a string. Which is not very convenient especially in the
        case of the "obj" attribute of Form, which is usually expected to be an
        Eloquent model (which implements __toString()).
        This is to enforce the explicit attribute and grab it before the
        improper serialization; it is then put again into the stack of
        properties of the instance by processParams()
    */
    public function __construct($obj = null)
    {
        $this->obj = $obj;
        parent::__construct();
    }

    public static function parameters()
    {
        return array_merge(parent::parameters(), [
            'obj' => (object) [
                'type' => 'object',
                'default' => null,
            ],

            /*
                label_width and input_width are usually meaningful for forms,
                but are applied here to be leveraged by any Container including
                inputs (e.g. a Collapse into a Form)
            */
            'label_width' => (object) [
                'type' => 'integer',
                'default' => 2,
            ],
            'input_width' => (object) [
                'type' => 'integer',
                'default' => 10,
            ],

            'appendNodes' => (object) [
                'type' => 'array',
                'default' => [],
            ],
        ]);
    }

    protected function processParams($params)
    {
        if ($this->obj) {
            $params['obj'] = $this->obj;
        }
        else {
            $this->obj = $params['obj'] ?? null;
        }

        return parent::processParams($params);
    }

    protected function setSlot($content)
    {
        $this->slot = $content;
    }

    protected function getSlot()
    {
        return $this->slot;
    }
}
