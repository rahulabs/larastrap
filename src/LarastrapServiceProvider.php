<?php

namespace MadBob\Larastrap;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;

use MadBob\Larastrap\Base\Stack;

class LarastrapServiceProvider extends ServiceProvider
{
    private $prefixes = ['larastrap'];

    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../config/larastrap.php',
            'larastrap'
        );

        $alternative = config('larastrap.prefix');
        if ($alternative && in_array($alternative, $this->prefixes) == false) {
            $this->prefixes[] = $alternative;
        }

        $this->app->singleton('LarastrapStack', function ($app) {
            $stack = new Stack();
            $stack->setPrefixes($this->prefixes);
            return $stack;
        });
    }

    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/../views', 'larastrap');

        $this->publishes([
            __DIR__ . '/../config/larastrap.php' => config_path('larastrap.php')
        ], 'config');

        $stack = app()->make('LarastrapStack');

        foreach($stack->getPrefixes() as $alternative) {
            Blade::componentNamespace('MadBob\\Larastrap\\Components', $alternative);
        }

        $customs = config('larastrap.customs');
        if ($customs) {
            foreach($customs as $tag => $config) {
                $stack->addCustomElement($tag, $config);
            }
        }
    }
}
