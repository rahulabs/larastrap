<?php

namespace MadBob\Larastrap\Components;

use MadBob\Larastrap\Base\Commons;

class Checklist extends Radiolist
{
    protected function processParams($params)
    {
        /*
            This is to enforce handling options as an HTML array
        */
        if (empty($params['npostfix'])) {
            $params['npostfix'] = '[]';
        }

        $params = parent::processParams($params);
        $params = Commons::processParamsAsInput($this, $params);
        $params = Commons::processParamsMultipleValues($params);

        return $params;
    }
}
