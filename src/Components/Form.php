<?php

namespace MadBob\Larastrap\Components;

use MadBob\Larastrap\Base\Container;

class Form extends Container
{
    public static function parameters()
    {
        return array_merge(parent::parameters(), [
            'formview' => (object) [
                'type' => 'enum:horizontal,vertical,inline,grid,squeeze',
                'default' => 'horizontal',
            ],
            'method' => (object) [
                'type' => 'string',
                'default' => '',
                'serialize' => true,
            ],
            'action' => (object) [
                'type' => 'string',
                'default' => '',
                'serialize' => true,
            ],
            'baseaction' => (object) [
                'type' => 'string',
                'default' => '',
            ],
            'gutter' => (object) [
                'type' => 'integer',
                'default' => 3,
            ],
            'enctype' => (object) [
                'type' => 'string',
                'default' => '',
                'serialize' => true,
            ],
            'client_side_errors' => (object) [
                'type' => 'boolean',
                'default' => false,
            ],
            'error_handling' => (object) [
                'type' => 'boolean',
                'default' => true,
            ],
            'error_bag' => (object) [
                'type' => 'string',
                'default' => 'default',
            ],
            'buttons_align' => (object) [
                'type' => 'enum:start,center,end',
                'default' => 'end',
            ],
            'buttons' => (object) [
                'type' => 'array',
                'default' => [
                    ['color' => 'primary', 'label' => 'Save', 'attributes' => ['type' => 'submit']]
                ]
            ],
            'keep_buttons' => (object) [
                'type' => 'boolean',
                'default' => false,
            ],
        ]);
    }

    protected function localAttributes()
    {
        return array_merge(parent::localAttributes(), ['buttons']);
    }

    protected function processParams($params)
    {
        $params = parent::processParams($params);
        $myclasses = [];

        if ($params['client_side_errors']) {
            $myclasses[] = 'needs-validation';
            $params['attributes'] = array_merge($params['attributes'] ?? [], ['novalidate' => true]);
        }

        switch($params['formview']) {
            case 'inline':
                $myclasses = ['row', 'row-cols-lg-auto', 'g-' . $params['gutter'], 'align-items-center'];
                break;
            case 'grid':
                $myclasses = ['row', 'g-' . $params['gutter']];
                break;
        }

        $params['classes'] = array_merge($params['classes'] ?? [], $myclasses);

        $method = strtoupper($params['method']);
        $params['real_method'] = $params['method'] = $method;
        if ($method != 'GET' && $method != 'POST') {
            $params['method'] = 'POST';
        }

        return $params;
    }

    protected function finalProcess($params)
    {
        if ($params['keep_buttons'] == false) {
            $parent = $this->closestParent(Modal::class);
            if ($parent) {
                $my_buttons = [];

                /*
                    This is to logically link the buttons to the form, as they
                    will be placed outside the form itself (more exactly, in the
                    modal's footer)
                */
                foreach($params['buttons'] as $btn) {
                    if (!isset($btn['form']) && !isset($btn['attributes']['form'])) {
                        $btn['attributes']['form'] = $params['id'];

                        /*
                            The explicit assignation of the contextual $obj to
                            the button is to leverage the ability to put any
                            kind of "element" in a "button", including those
                            which may require some contextual information
                        */
                        $btn['obj'] = $params['obj'];
                    }

                    $my_buttons[] = $btn;
                }

                $modal = $this->getStack()->getStatus($parent);
                $modal_buttons = array_merge($modal['buttons'], $my_buttons);
                $this->getStack()->updateStatus($parent, ['buttons' => $modal_buttons]);
                $params['buttons'] = [];
            }
        }

        if (blank($params['action'])) {
            if (filled($params['baseaction'])) {
                $prefix = $params['baseaction'];

                if ($params['obj'] && is_object($params['obj']) && (is_a($params['obj'], \Illuminate\Database\Eloquent\Model::class) == false || $params['obj']->exists)) {
                    $params['action'] = route($prefix . '.update', $params['obj']->id);
                    $params['real_method'] = 'PUT';
                }
                else {
                    $params['action'] = route($prefix . '.store');
                }
            }
        }

        foreach($params['buttons'] as $k => $v) {
            $type = $params['buttons'][$k]['element'] ?? 'button';
            $params['buttons'][$k]['element'] = str_contains($type, '::') ? $type : sprintf('larastrap::%s', $type);
        }

        return parent::finalProcess($params);
    }
}
