<?php

namespace MadBob\Larastrap\Components;

class Datetime extends Input
{
    protected function inputType()
    {
        return 'datetime-local';
    }
}
