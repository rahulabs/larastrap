<?php

namespace MadBob\Larastrap\Components;

class Password extends Input
{
    protected function inputType()
    {
        return 'password';
    }

    protected function processParams($params)
    {
        $params = parent::processParams($params);

        /*
            By default, if the password value is read from the object it is
            wiped to avoid unwanted leaks
        */
        if ($params['value_from_object']) {
            $params['value'] = '';
        }

        return $params;
    }
}
