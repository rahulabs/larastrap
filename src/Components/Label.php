<?php

namespace MadBob\Larastrap\Components;

use MadBob\Larastrap\Base\Element;

class Label extends Element
{
    public static function parameters()
    {
        return array_merge(parent::parameters(), [
            'label' => (object) [
                'type' => 'string',
                'default' => '',
            ],
            'tlabel' => (object) [
                'type' => 'string',
                'translates' => 'label',
                'default' => '',
            ],
            'label_html' => (object) [
                'type' => 'html_version',
                'default' => '',
                'to' => 'label',
            ],
            'pophelp' => (object) [
                'type' => 'string',
                'default' => null,
            ],
            'tpophelp' => (object) [
                'type' => 'string',
                'translates' => 'pophelp',
                'default' => '',
            ],
            'for' => (object) [
                'type' => 'string',
                'default' => '',
            ],
        ]);
    }
}
