<?php

namespace MadBob\Larastrap\Components;

class Tel extends Input
{
    protected function inputType()
    {
        return 'tel';
    }
}
