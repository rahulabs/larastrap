<?php

namespace MadBob\Larastrap\Components;

use MadBob\Larastrap\Base\Container;

class Field extends Container
{
    public static function parameters()
    {
        return array_merge(parent::parameters(), [
            'label' => (object) [
                'type' => 'string',
                'default' => '',
            ],
            'tlabel' => (object) [
                'type' => 'string',
                'translates' => 'label',
                'default' => '',
            ],
            'label_html' => (object) [
                'type' => 'html_version',
                'default' => '',
                'to' => 'label',
            ],
            'label_width' => (object) [
                'type' => 'integer|array',
                'default' => 2,
            ],
            'input_width' => (object) [
                'type' => 'integer|array',
                'default' => 10,
            ],
            'label_class' => (object) [
                'type' => 'string_array',
                'default' => [],
            ],
            'do_label' => (object) [
                'type' => 'enum:show,hide,empty,squeeze',
                'default' => 'show',
            ],
            'help' => (object) [
                'type' => 'string',
                'default' => null,
            ],
            'thelp' => (object) [
                'type' => 'string',
                'translates' => 'help',
                'default' => '',
            ],
            'pophelp' => (object) [
                'type' => 'string',
                'default' => null,
            ],
            'tpophelp' => (object) [
                'type' => 'string',
                'translates' => 'pophelp',
                'default' => '',
            ],
            'squeeze' => (object) [
                'type' => 'boolean',
                'default' => false,
            ],
        ]);
    }

    public function render()
    {
        return function (array $data) {
            $params = $this->generateParams(true);

            /*
                If the child input is "squeezed", the field has not to be
                displayed. In this case just the internal slot is returned.
                The attribute "squeeze" is set up to the parent in
                Commons::processParamsAsInput()
            */
            if ($params['squeeze'] == true) {
                $this->getStack()->popStatus();
                return (string) $data['slot'];
            }
            else {
                return parent::render()($data);
            }
        };
    }

    private function breakpointWidths($config)
    {
        if (is_array($config)) {
            $width_class = [];

            foreach($config as $breakpoint => $width) {
                if ($breakpoint == 'xs') {
                    $width_class[] = sprintf('col-%d', $width);
                }
                else {
                    $width_class[] = sprintf('col-%s-%d', $breakpoint, $width);
                }
            }

            $width_class = join(' ', $width_class);
        }
        else {
            $width_class = sprintf('col-%d', $config);
        }

        return $width_class;
    }

    protected function processParams($params)
    {
        $params = parent::processParams($params);

        $input_wrap_tag_start = '';
        $input_wrap_tag_end = '';

        $label_width_class = $this->breakpointWidths($params['label_width']);
        $input_width_class = $this->breakpointWidths($params['input_width']);
        $label_class = [];

        switch($params['formview'] ?? 'horizontal') {
            case 'horizontal':
                $main_wrap_class = 'row mb-3';
                $label_class = [$label_width_class, 'col-form-label'];
                $input_wrap_tag_start = '<div class="' . $input_width_class . '">';
                $input_wrap_tag_end = '</div>';
                break;

            case 'vertical':
                $main_wrap_class = 'mb-3';
                $label_class = ['form-label'];
                break;

            case 'grid':
                $main_wrap_class = $input_width_class;
                $label_class = ['form-label'];
                break;

            case 'inline':
                $main_wrap_class = 'col-auto';
                $params['do_label'] = 'hide';
                break;

            case 'squeeze':
                $main_wrap_class = '';
                $params['do_label'] = 'squeeze';
                break;
        }

        $params['classes'][] = $main_wrap_class;

        /*
            Parameters intended to the label are moved in a dedicated array, to
            be then passed in block to the child x-larastrap::label widget
            formatted into the field
        */
        $params['label_params'] = array_merge($params['label_params'] ?? [], [
            'label' => $params['label'],
            'label_html' => $params['label_html'],
            'classes' => $params['label_class'],
            'pophelp' => $params['pophelp'],
            'for' => $params['id'],
        ]);

        $params['label_params']['classes'] = array_merge($params['label_params']['classes'], $label_class);

        /*
            The do_label parameters appears in multiple places.
            If "empty", here we override the label to an empty string. This
            implies that the label is still appended to the DOM, and occupies
            some space, but contains nothing.
            If "hide", the label is not appended at all to the DOM.
            By default, is is appended within the field with the required value.
        */
        if ($params['do_label'] == 'empty') {
            $params['label_params']['label'] = '';
        }

        /*
            do_label="squeeze" acts as the squeeze attribute
        */
        if ($params['do_label'] == 'squeeze') {
            $params['squeeze'] = true;
        }

        $params['input_wrap_tag_start'] = ' ' . $input_wrap_tag_start;
        $params['input_wrap_tag_end'] = ' ' . $input_wrap_tag_end;

        return $params;
    }
}
