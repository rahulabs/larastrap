<?php

namespace MadBob\Larastrap\Components;

class Search extends Input
{
    protected function inputType()
    {
        return 'search';
    }
}
