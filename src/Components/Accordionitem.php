<?php

namespace MadBob\Larastrap\Components;

use MadBob\Larastrap\Base\Container;

class Accordionitem extends Container
{
    public static function parameters()
    {
        return array_merge(parent::parameters(), [
            'active' => (object) [
                'type' => 'boolean',
                'default' => false,
            ],
            'label' => (object) [
                'type' => 'string',
                'default' => '',
            ],
            'tlabel' => (object) [
                'type' => 'string',
                'translates' => 'label',
                'default' => '',
            ],
            'label_html' => (object) [
                'type' => 'html_version',
                'default' => '',
                'to' => 'label',
            ],
        ]);
    }

    protected function processParams($params)
    {
        $params = parent::processParams($params);

        $params['parent_link'] = '';
        if ($params['always_open'] == false) {
            $accordion = $this->closestParent(Accordion::class);
            if ($accordion) {
                $accordion_config = $this->getStack()->getStatus($accordion);
                $params['parent_link'] = sprintf('data-bs-parent="#%s"', $accordion_config['id']);
            }
        }

        return $params;
    }

    protected function baseClass()
    {
        return 'accordion-item';
    }
}
