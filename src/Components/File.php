<?php

namespace MadBob\Larastrap\Components;

use MadBob\Larastrap\Base\Commons;

class File extends Input
{
    public static function parameters()
    {
        return array_merge(parent::parameters(), [
            'multiple' => (object) [
                'type' => 'boolean',
                'default' => false,
                'serialize' => true,
            ],
        ]);
    }

    protected function inputType()
    {
        return 'file';
    }

    protected function processParams($params)
    {
        /*
            This is to enforce handling options as an HTML array
        */
        if ($params['multiple'] && empty($params['npostfix']) && substr($params['name'], -2) != '[]') {
            $params['npostfix'] = '[]';
        }

        $params = parent::processParams($params);

        $parent = $this->closestParent(Form::class);
        if ($parent) {
            $this->getStack()->updateStatus($parent, ['enctype' => 'multipart/form-data']);
        }


        return $params;
    }
}
